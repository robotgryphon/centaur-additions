package io.tedsenft.tauradditions.common.model;

import io.tedsenft.tauradditions.Reference;
import io.tedsenft.tauradditions.common.items.ItemHelper;
import net.minecraft.block.Block;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;

/**
 * Created by Ted on 2/4/2016.
 */
public class ModelHelper {

    public static void registerModel(Block b){
        registerModel(b, "inventory");
    }

    public static void registerModel(Block b, String variant){
        Item i = Item.getItemFromBlock(b);
        ModelLoader.setCustomModelResourceLocation(i, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + b.getUnlocalizedName().substring(5), variant));
    }

    public static void registerModel(Item i){
        registerModel(i, 0, "inventory");
    }

    public static void registerModel(Item i, int meta){
        registerModel(i, meta, "inventory");
    }

    public static void registerModel(Item i, int meta, String variant){
        ModelLoader.setCustomModelResourceLocation(i, meta, new ModelResourceLocation(Reference.MOD_ID + ":" + i.getUnlocalizedName().substring(5), variant));
    }
}
