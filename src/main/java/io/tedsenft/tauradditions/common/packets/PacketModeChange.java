package io.tedsenft.tauradditions.common.packets;

import io.netty.buffer.ByteBuf;
import io.tedsenft.tauradditions.LogHelper;
import io.tedsenft.tauradditions.common.items.ItemMultitool;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

/**
 * Created by Ted on 2/8/2016.
 */
public class PacketModeChange implements IMessage {

    public ItemStack stack;

    public PacketModeChange(){

    }

    public PacketModeChange(ItemStack s){
        this.stack = s;
    }

    /**
     * Convert from the supplied buffer into your specific message type
     *
     * @param buf
     */
    @Override
    public void fromBytes(ByteBuf buf) {
        stack = ByteBufUtils.readItemStack(buf);
    }

    /**
     * Deconstruct your message into the supplied byte buffer
     *
     * @param buf
     */
    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeItemStack(buf, stack);
    }

    public static class Handler implements IMessageHandler<PacketModeChange, IMessage> {

        @Override
        public IMessage onMessage(final PacketModeChange packet, MessageContext ctx) {
            MinecraftServer.getServer().addScheduledTask(() -> handlePacket(packet, ctx));
            return null;
        }

        private void handlePacket(PacketModeChange packet, MessageContext ctx) {
            LogHelper.debug("Recieved packet from client, running ItemMultitool.switchFunction...");
            ((ItemMultitool) packet.stack.getItem()).switchFunction(packet.stack);
        }
    }
}
