package io.tedsenft.tauradditions.common.items;

import com.mojang.realmsclient.gui.ChatFormatting;
import io.tedsenft.tauradditions.TaurAdditions;
import io.tedsenft.tauradditions.TaurAdditionsTab;
import io.tedsenft.tauradditions.common.blocks.BlockHelper;
import io.tedsenft.tauradditions.LogHelper;
import net.minecraft.block.BlockFence;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

import java.util.List;

/**
 * Created by Ted on 2/1/2016.
 */
public class ItemHammockSling extends Item {

    public ItemHammockSling(){
        super();

        setCreativeTab(TaurAdditionsTab.instance);
        setMaxStackSize(1);
        setUnlocalizedName("hammock.sling");
    }

    // Add first hit information to tooltip if needed
    @Override
    public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
        if(stack.getTagCompound() != null){
            NBTTagCompound tags = stack.getTagCompound();
            if(tags.hasKey("first_hit")){
                NBTTagCompound fh = tags.getCompoundTag("first_hit");
                tooltip.add(
                        ChatFormatting.WHITE +
                        "First Hit: " +
                        ChatFormatting.LIGHT_PURPLE +

                        fh.getInteger("x") + ", " +
                        fh.getInteger("y") + ", " +
                        fh.getInteger("z")
                );
            }
        }
    }

    @Override
    public boolean onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ) {

        // If on server
        if(!worldIn.isRemote){

            NBTTagCompound tags = stack.getTagCompound();
            if(tags == null) {
                stack.setTagCompound(new NBTTagCompound());
                tags = stack.getTagCompound();

                NBTTagCompound firstHit = new NBTTagCompound();
                firstHit.setInteger("x", pos.getX());
                firstHit.setInteger("y", pos.getY());
                firstHit.setInteger("z", pos.getZ());

                tags.setTag("first_hit", firstHit);
                stack.setTagCompound(tags);
                return true;
            }

            IBlockState state = worldIn.getBlockState(pos);
            if(state.getBlock() instanceof BlockFence){

                if(tags.hasKey("first_hit")){

                    NBTTagCompound fh = tags.getCompoundTag("first_hit");
                    BlockPos firstHit = new BlockPos(
                            fh.getInteger("x"),
                            fh.getInteger("y"),
                            fh.getInteger("z")
                    );

                    double dist = pos.distanceSq(firstHit);
                    LogHelper.info(dist);

                    if(dist == 1){
                        // Delete item
                        stack.stackSize--;

                        int compareX = Integer.compare(pos.getX(), firstHit.getX());
                        int compareZ = Integer.compare(pos.getZ(), firstHit.getZ());

                        // Somehow block distance is zero still?!
                        if(compareX == 0 && compareZ == 0)
                            return false;

                        // Diagonal, somehow
                        if(compareX != 0 && compareZ != 0)
                            return false;

                        IBlockState hammockPart = BlockHelper.hammock.getDefaultState();

                        worldIn.setBlockState(firstHit, hammockPart);
                        worldIn.setBlockState(pos, hammockPart);

                        return true;
                    }
                    return true;
                } else {

                    NBTTagCompound firstHit = new NBTTagCompound();
                    firstHit.setInteger("x", pos.getX());
                    firstHit.setInteger("y", pos.getY());
                    firstHit.setInteger("z", pos.getZ());

                    tags.setTag("first_hit", firstHit);
                    stack.setTagCompound(tags);

                    return true;
                }
            }
        }

        return false;
    }
}
