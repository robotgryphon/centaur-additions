package io.tedsenft.tauradditions.common.items;

import com.mojang.realmsclient.gui.ChatFormatting;
import io.tedsenft.tauradditions.LogHelper;
import io.tedsenft.tauradditions.TaurAdditions;
import io.tedsenft.tauradditions.TaurAdditionsTab;
import io.tedsenft.tauradditions.common.items.multitool.MultitoolHelper;
import io.tedsenft.tauradditions.common.items.multitool.IMultitoolMethods;
import io.tedsenft.tauradditions.common.items.multitool.MultitoolFunction;
import io.tedsenft.tauradditions.common.packets.PacketModeChange;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.command.CommandTitle;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;

/**
 * Created by Ted on 2/2/2016.
 */
public class ItemMultitool extends Item {

    public ItemMultitool(){
        super();

        setUnlocalizedName("multitool");
        setMaxStackSize(1);
        setCreativeTab(TaurAdditionsTab.instance);
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
        if(GuiScreen.isShiftKeyDown()) {
            tooltip.add("Current Function: " + ChatFormatting.LIGHT_PURPLE + MultitoolHelper.getFunctionName(getCurrentFunction(stack)));
        } else tooltip.add("Hold Shift for more information.");
    }

    public void switchFunction(ItemStack s){

        String curFunction = getCurrentFunction(s);

        LogHelper.info("Function on server before modification: " + curFunction);

        // Create handler and bump call to server.
        if(MultitoolHelper.functionListHasEntries()) {
            // Now switch functions
            String nextFunction = MultitoolHelper.getNextFunction(curFunction);
            LogHelper.info("Modifying nbt data to change function to " + nextFunction);


            NBTTagCompound tags = s.getTagCompound();
            tags.setString("function", nextFunction);

            s.setTagCompound(tags);
        }
    }

    public String getCurrentFunction(ItemStack s){
        if(s.getTagCompound() == null)
            s.setTagCompound(new NBTTagCompound());

        if(!s.getTagCompound().hasKey("function")) {
            s.getTagCompound().setString("function", "none");
            return "none";
        }

        return s.getTagCompound().getString("function");
    }

    @Override
    public boolean onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ) {

        // Make sure we're doing stuffs on the server

        String curFunction = getCurrentFunction(stack);

        playerIn.addChatComponentMessage( new ChatComponentText( (worldIn.isRemote ? "Client: " : "Server: ") + curFunction) );
        return false;
    }
}
