package io.tedsenft.tauradditions.common.items.multitool.functions;

import io.tedsenft.tauradditions.LogHelper;
import io.tedsenft.tauradditions.common.items.multitool.IMultitoolMethods;
import io.tedsenft.tauradditions.common.items.multitool.MultitoolFunction;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

/**
 * Created by Ted on 2/8/2016.
 */
@MultitoolFunction(unlocalName = "function.link", requiresSneaking = true)
public class FunctionLink implements IMultitoolMethods {

    /***
     * Used by reflection to start tool function.
     *
     * @param world
     * @param player
     * @param params Required passed parameters  @return True if function executed successfully, false otherwise.
     */
    @Override
    public boolean act(ItemStack stack, World world, EntityPlayer player, NBTTagCompound params) {
        LogHelper.info("Running function act in FunctionLink.");
        return true;
    }
}
