package io.tedsenft.tauradditions.common.items.multitool;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

/**
 * Created by Ted on 2/8/2016.
 */
public interface IMultitoolMethods {

    boolean act(ItemStack stack, World world, EntityPlayer player, NBTTagCompound data);

}
