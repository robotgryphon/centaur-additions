package io.tedsenft.tauradditions.common.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;

/**
 * Created by Ted on 1/28/2016.
 */
public class ItemRope extends Item {

    private BlockPos firstHit;

    public ItemRope(){
        super();
        setUnlocalizedName("rope");

        setCreativeTab(CreativeTabs.tabMaterials);
    }


}
