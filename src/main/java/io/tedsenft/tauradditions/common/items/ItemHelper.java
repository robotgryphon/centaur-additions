package io.tedsenft.tauradditions.common.items;

import io.tedsenft.tauradditions.Reference;
import io.tedsenft.tauradditions.common.model.ModelHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

/**
 * Created by Ted on 1/28/2016.
 */
public class ItemHelper {

    public static ItemFlightRing ring;
    public static ItemRope rope;
    public static ItemHammockSling sling;
    public static ItemMultitool tool;

    public static void registerItems(){
        ring = new ItemFlightRing();
        rope = new ItemRope();
        sling = new ItemHammockSling();
        tool = new ItemMultitool();

        GameRegistry.registerItem(ring, "ring.flight");
        GameRegistry.registerItem(rope, "rope");
        GameRegistry.registerItem(tool, "multitool");
        GameRegistry.registerItem(sling, "hammock.sling");
    }

    public static void createRecipes(){
        GameRegistry.addRecipe(new ShapelessOreRecipe(
                new ItemStack(rope, 1),
                Items.string,
                Items.string,
                Items.string) );

        GameRegistry.addRecipe(new ShapedOreRecipe(
                new ItemStack(sling),
                true,
                new Object[]{
                        "r r",
                        "ccc",
                        'r', rope,
                        'c', Blocks.carpet
                }
        ));
    }

    public static void registerItemModels(){
        ModelHelper.registerModel(ItemHelper.ring);
        ModelHelper.registerModel(ItemHelper.rope);
        ModelHelper.registerModel(ItemHelper.sling);
    }
}
