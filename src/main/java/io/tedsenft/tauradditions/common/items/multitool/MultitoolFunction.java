package io.tedsenft.tauradditions.common.items.multitool;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Ted on 2/8/2016.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MultitoolFunction {

    String unlocalName();

    /***
     * Whether or not the player needs to sneak to use the function.
     * Probably going to be used if you're interacting with blocks.
     * @return
     */
    boolean requiresSneaking();

}
