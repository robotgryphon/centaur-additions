package io.tedsenft.tauradditions.common.items.multitool;

import io.tedsenft.tauradditions.LogHelper;
import io.tedsenft.tauradditions.common.items.multitool.functions.FunctionLink;
import io.tedsenft.tauradditions.common.items.multitool.functions.FunctionUnlink;
import net.minecraft.util.StatCollector;

import java.util.*;

/**
 * Created by Ted on 2/8/2016.
 */
public class MultitoolHelper {

    private static LinkedList<String> functionMap;

    public static String getFunctionName(String className){
        if(!functionMap.contains(className))
            return "NOT_VALID";

        try {
            Class c = Class.forName(className);
            if(!c.isAnnotationPresent(MultitoolFunction.class))
                return StatCollector.translateToLocal("multitool.missing_function_annote");

            MultitoolFunction mtf = (MultitoolFunction) c.getAnnotation(MultitoolFunction.class);
            if(mtf.unlocalName() != null)
                return StatCollector.translateToLocal(mtf.unlocalName());

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return "NOT_FOUND";
    }

    public static boolean functionListHasEntries(){
        return !functionMap.isEmpty();
    }

    public static String getNextFunction(String current){
        int curIndex = functionMap.indexOf(current);
        if(curIndex == -1) {
            LogHelper.info("No entry found with that classname, trying to reload first available tool function..");
            if (!functionMap.isEmpty())
                return functionMap.get(0);
            else
                return "none";
        }

        if(curIndex + 1 == functionMap.size())
            return functionMap.get(0);
        else
            return functionMap.get(curIndex+1);
    }

    public static void registerEmbeddedFunctions(){
        functionMap = new LinkedList<String>();
        functionMap.add(FunctionLink.class.getName());
        functionMap.add(FunctionUnlink.class.getName());
    }
}
