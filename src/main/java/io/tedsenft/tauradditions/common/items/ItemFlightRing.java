package io.tedsenft.tauradditions.common.items;

import baubles.api.BaubleType;
import baubles.api.IBauble;
import cofh.api.energy.IEnergyContainerItem;
import com.mojang.realmsclient.gui.ChatFormatting;
import io.tedsenft.tauradditions.TaurAdditions;
import io.tedsenft.tauradditions.LogHelper;
import io.tedsenft.tauradditions.TaurAdditionsTab;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import java.util.List;

/**
 * Created by Ted on 1/28/2016.
 */
public class ItemFlightRing extends Item implements IBauble {

    public ItemFlightRing(){
        super();
        setMaxStackSize(1);
        this.setUnlocalizedName("ring.flight");
        setCreativeTab(TaurAdditionsTab.instance);
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced) {
        tooltip.add(ChatFormatting.LIGHT_PURPLE + "Equip in a ring slot.");
    }

    /**
     * This method return the type of bauble this is.
     * Type is used to determine the slots it can go into.
     *
     * @param itemstack
     */
    @Override
    public BaubleType getBaubleType(ItemStack itemstack) {
        return BaubleType.RING;
    }

    @Override
    public void onWornTick(ItemStack itemstack, EntityLivingBase player) {
        // TODO: Implement charge system
    }

    @Override
    public void onEquipped(ItemStack itemstack, EntityLivingBase playerEntity) {
        EntityPlayer player = (EntityPlayer) playerEntity;

        if(player.worldObj.isRemote)
            LogHelper.info(player.capabilities.allowFlying);

        if(!player.capabilities.allowFlying)
            player.capabilities.allowFlying = true;
    }

    @Override
    public void onUnequipped(ItemStack itemstack, EntityLivingBase playerEntity) {
        EntityPlayer player = (EntityPlayer) playerEntity;
        player.capabilities.allowFlying = false;
        player.capabilities.isFlying = false;
    }

    @Override
    public boolean canEquip(ItemStack itemstack, EntityLivingBase player) {

        return true;
    }

    @Override
    public boolean canUnequip(ItemStack itemstack, EntityLivingBase player) {
        return true;
    }
    //endregion;


    @Override
    public boolean onDroppedByPlayer(ItemStack item, EntityPlayer player) {
        player.capabilities.allowFlying = false;
        player.capabilities.isFlying = false;
        return true;
    }

    @Override
    public void onCreated(ItemStack stack, World worldIn, EntityPlayer playerIn) {
        playerIn.capabilities.allowFlying = true;
    }
}
