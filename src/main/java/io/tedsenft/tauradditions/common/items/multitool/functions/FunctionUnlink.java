package io.tedsenft.tauradditions.common.items.multitool.functions;

import io.tedsenft.tauradditions.LogHelper;
import io.tedsenft.tauradditions.common.items.multitool.IMultitoolMethods;
import io.tedsenft.tauradditions.common.items.multitool.MultitoolFunction;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

/**
 * Created by Ted on 2/8/2016.
 */
@MultitoolFunction(unlocalName = "function.unlink", requiresSneaking = true)
public class FunctionUnlink implements IMultitoolMethods {
    @Override
    public boolean act(ItemStack stack, World world, EntityPlayer player, NBTTagCompound data) {
        LogHelper.info("Unlinking blocks");
        return true;
    }
}
