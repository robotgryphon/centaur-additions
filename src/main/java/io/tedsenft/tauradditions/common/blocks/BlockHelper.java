package io.tedsenft.tauradditions.common.blocks;

import io.tedsenft.tauradditions.common.model.ModelHelper;
import io.tedsenft.tauradditions.common.tiles.TileCharger;
import io.tedsenft.tauradditions.common.tiles.TileSwapper;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.entity.player.PlayerDropsEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by Ted on 1/28/2016.
 */
public class BlockHelper {

    public static BlockSwapper swapper;
    public static BlockHammock hammock;
    public static BlockCharger charger;
    public static BlockEnergyNode energyNode;

    public static void registerBlocks(){
        swapper = new BlockSwapper();
        hammock = new BlockHammock();
        charger = new BlockCharger();
        energyNode = new BlockEnergyNode();

        GameRegistry.registerBlock(swapper, "swapper");
        GameRegistry.registerBlock(hammock, "hammock");
        GameRegistry.registerBlock(charger, "charger");
        GameRegistry.registerBlock(energyNode);
    }

    public static void createRecipes(){
        
    }

    public static void registerBlockModels(){
        ModelHelper.registerModel(hammock, "occupied=false,facing=north");
        ModelHelper.registerModel(swapper, "enabled=true");
    }
}
