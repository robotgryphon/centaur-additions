package io.tedsenft.tauradditions.common.blocks;

import io.tedsenft.tauradditions.TaurAdditionsTab;
import io.tedsenft.tauradditions.common.tiles.TileEnergyNode;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.IStringSerializable;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.b3d.B3DLoader;
import net.minecraftforge.common.property.ExtendedBlockState;
import net.minecraftforge.common.property.IExtendedBlockState;
import net.minecraftforge.common.property.IUnlistedProperty;

/**
 * Created by Ted on 2/13/2016.
 */
public class BlockEnergyNode extends Block {

    public enum EnergyNodeType implements IStringSerializable {

        INPUT(0, "input"),
        OUTPUT(1, "output"),
        LINKING(2, "linking");

        private String name;
        private int id;

        private EnergyNodeType(int id, String name){
            this.id = id;
            this.name = name;
        }

        @Override
        public String getName() { return name; }
        public int getID(){ return id; }
    }

    public ExtendedBlockState state = new ExtendedBlockState(this, new IProperty[]{ MODE }, new IUnlistedProperty[]{B3DLoader.B3DFrameProperty.instance});

    public static final PropertyEnum<EnergyNodeType> MODE = PropertyEnum.create("mode", EnergyNodeType.class);
    protected int counter = 1;

    public BlockEnergyNode(){
        super(Material.wood);
        setUnlocalizedName("node.energy");
        setRegistryName("node.energy");
        setCreativeTab(TaurAdditionsTab.instance);
        setBlockBounds(0.375f, 0, 0.375f, 0.625f, 0.4f, 0.625f);

        setDefaultState(this.getDefaultState().withProperty(MODE, EnergyNodeType.LINKING));
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean isNormalCube() {
        return false;
    }

    @Override
    public boolean isNormalCube(IBlockAccess world, BlockPos pos) {
        return false;
    }

    @Override
    protected BlockState createBlockState() {
        return new ExtendedBlockState(this, new IProperty[]{MODE}, new IUnlistedProperty[]{B3DLoader.B3DFrameProperty.instance});
    }

    @Override
    public TileEntity createTileEntity(World world, IBlockState state) {
        return new TileEnergyNode();
    }

    @Override
    public boolean hasTileEntity(IBlockState state) {
        return true;
    }

    @Override
    public IBlockState getExtendedState(IBlockState state, IBlockAccess world, BlockPos pos) {
        B3DLoader.B3DState newState = new B3DLoader.B3DState(null, counter);
        return ((IExtendedBlockState) state).withProperty(B3DLoader.B3DFrameProperty.instance, newState);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        EnergyNodeType type;
        switch (meta) {
            case 0:
                type = EnergyNodeType.INPUT;
                break;
            case 1:
                type = EnergyNodeType.OUTPUT;
                break;
            case 2:
                type = EnergyNodeType.LINKING;
                break;

            default:
                type = EnergyNodeType.LINKING;
                break;
        }

        IBlockState state = this.getDefaultState()
            .withProperty(MODE, type);

        return state;
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(MODE).getID();
    }

}
