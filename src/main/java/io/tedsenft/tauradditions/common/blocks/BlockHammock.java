package io.tedsenft.tauradditions.common.blocks;

import io.tedsenft.tauradditions.TaurAdditions;
import io.tedsenft.tauradditions.TaurAdditionsTab;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerSleepInBedEvent;

/**
 * Created by Ted on 1/28/2016.
 */
public class BlockHammock extends Block {

    public static PropertyDirection Facing = PropertyDirection.create("facing");

    public static PropertyBool Occupied = PropertyBool.create("occupied");

    public BlockHammock(){
        super(Material.cloth);
        setBlockBounds(0, 0, 0, 1, 0.7f, 1);
        setUnlocalizedName("hammock");
        setHardness(0.2f);
        setCreativeTab(TaurAdditionsTab.instance);

        IBlockState def = this.getDefaultState().withProperty(Occupied, false);

        this.setDefaultState(def);
    }

    @Override
    public boolean isOpaqueCube()
    {
        return false;
    }

    @Override
    public boolean isFullCube()
    {
        return false;
    }

    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {

        EnumFacing[] tests = new EnumFacing[]{ EnumFacing.NORTH, EnumFacing.SOUTH, EnumFacing.WEST, EnumFacing.EAST };
        for (EnumFacing test : tests) {
            if(worldIn.getBlockState(pos.offset(test)).getBlock() instanceof BlockHammock) {

                return state.withProperty(Facing, test);

            }
        }

        return state;
    }

    @Override
    public void onBlockDestroyedByPlayer(World worldIn, BlockPos pos, IBlockState state) {
        if(!worldIn.isRemote) {
            EnumFacing f = state.getValue(Facing);
            worldIn.setBlockToAir(pos.offset(f));
        }
    }

    @Override
    public boolean isBed(IBlockAccess world, BlockPos pos, Entity player) {
        return true;
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ) {
        if(worldIn.isRemote) {
            return true;
        }

        if(!playerIn.isSneaking() && !state.getValue(Occupied)){

            PlayerSleepInBedEvent evt = new PlayerSleepInBedEvent(playerIn, pos);
            MinecraftForge.EVENT_BUS.post(evt);

            switch(evt.getResult()){
                case ALLOW:
                    playerIn.renderOffsetZ = 90F;
                    playerIn.setPositionAndUpdate(pos.getX(), pos.getY(), pos.getZ());
                    break;

                case DENY:
                    return false;
            }
        }


        return true;
    }

    @Override
    public boolean canPlaceBlockAt(World worldIn, BlockPos pos) {
        // Check for other neighboring hammock parts, if found false
        return true;
    }

    @Override
    protected BlockState createBlockState() {
        return new BlockState(this, new IProperty[]{ Occupied, Facing });
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        IBlockState state = this.getDefaultState()
                .withProperty(Occupied, meta == 1);

        return state;
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(Occupied) ? 1 : 0;
    }
}
