package io.tedsenft.tauradditions.common.blocks;

import io.tedsenft.tauradditions.TaurAdditions;
import io.tedsenft.tauradditions.TaurAdditionsTab;
import io.tedsenft.tauradditions.common.tiles.TileCharger;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.b3d.B3DLoader;
import net.minecraftforge.common.property.ExtendedBlockState;
import net.minecraftforge.common.property.IExtendedBlockState;
import net.minecraftforge.common.property.IUnlistedProperty;

/**
 * Created by Ted on 2/3/2016.
 */
public class BlockCharger extends BlockMachine {

    public ExtendedBlockState state = new ExtendedBlockState(this, new IProperty[]{ }, new IUnlistedProperty[]{B3DLoader.B3DFrameProperty.instance});
    private int counter = 1;

    public BlockCharger(){
        super();
        setUnlocalizedName("charger");
        setCreativeTab(TaurAdditionsTab.instance);
    }

    @Override
    public TileEntity createTileEntity(World world, IBlockState state) {
        return new TileCharger();
    }

    @Override
    public boolean isNormalCube() {
        return false;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean hasTileEntity(IBlockState state) {
        return state.getBlock() instanceof BlockCharger;
    }
    @Override
    public IBlockState getExtendedState(IBlockState state, IBlockAccess world, BlockPos pos) {
        //Only return an IExtendedBlockState from this method and createState(), otherwise block placement might break!
        B3DLoader.B3DState newState = new B3DLoader.B3DState(null, counter);
        return ((IExtendedBlockState) state).withProperty(B3DLoader.B3DFrameProperty.instance, newState);
    }

    @Override
    public BlockState createBlockState()
    {
        return new ExtendedBlockState(this, new IProperty[]{ }, new IUnlistedProperty[]{B3DLoader.B3DFrameProperty.instance});
    }


}
