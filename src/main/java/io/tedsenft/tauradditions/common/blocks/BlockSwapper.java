package io.tedsenft.tauradditions.common.blocks;

import io.tedsenft.tauradditions.Reference;
import io.tedsenft.tauradditions.TaurAdditions;
import io.tedsenft.tauradditions.TaurAdditionsTab;
import io.tedsenft.tauradditions.common.tiles.TileSwapper;
import io.tedsenft.tauradditions.utils.NBTUtils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRedstoneTorch;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.*;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;

import java.sql.Ref;

/**
 * Created by Ted on 1/22/2016.
 */
public class BlockSwapper extends BlockMachine {

    public BlockSwapper() {
        super();
        this.setUnlocalizedName("swapper");
        this.setBlockBounds(0, 0, 0, 1, 0.125f, 1);
        this.setCreativeTab(TaurAdditionsTab.instance);
    }

    @Override
    public boolean isOpaqueCube()
    {
        return false;
    }

    @Override
    public boolean isFullCube()
    {
        return false;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox(World worldIn, BlockPos pos, IBlockState state) {
        return AxisAlignedBB.fromBounds(0, 0, 0, 1, 0.4d, 1);
    }

    @Override
    public void onEntityCollidedWithBlock(World worldIn, BlockPos pos, IBlockState state, Entity entityIn) {
        if(!worldIn.isRemote){
            if(entityIn instanceof EntityPlayer){
                // Do checks!
                EntityPlayer ep = (EntityPlayer) entityIn;

                TileEntity te = worldIn.getTileEntity(pos);
                if(te != null && te instanceof TileSwapper){
                    TileSwapper.TeleportResult tr = ((TileSwapper) te).tryTeleport(worldIn, ep);
                    if(tr == TileSwapper.TeleportResult.SUCCESS){
                        ep.addChatComponentMessage(new ChatComponentTranslation("tauradditions.swapper.whoosh"));
                    }
                }
            }
        }
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ) {
        // TODO: Implement gui
        if(!worldIn.isRemote) {

            ItemStack heldItem = playerIn.getHeldItem();
            if(heldItem == null)
                return false;

            Item held = heldItem.getItem();
            if(held.getRegistryName().equals(Items.stick.getRegistryName())){
                NBTTagCompound stickData = heldItem.getTagCompound();
                if(stickData == null || !stickData.hasKey("link_position")){
                    if(stickData == null){
                        stickData = new NBTTagCompound();

                        heldItem.setTagCompound(stickData);
                    }

                    NBTTagCompound posTag = NBTUtils.getTagFromPosition(pos);
                    stickData.setTag("link_position", posTag);

                    playerIn.addChatComponentMessage(new ChatComponentTranslation("tauradditions.swapper.linking"));

                } else {
                    // Try to get block state

                    BlockPos linkTo = NBTUtils.getPositionFromTag(stickData.getCompoundTag("link_position"));

                    TileSwapper ts1 = (TileSwapper) worldIn.getTileEntity(linkTo);
                    TileSwapper ts2 = (TileSwapper) worldIn.getTileEntity(pos);

                    if(ts1 == null || ts2 == null)
                        return false;

                    ts1.setLinkPosition(pos);
                    ts2.setLinkPosition(linkTo);

                    stickData.removeTag("link_position");

                    playerIn.addChatComponentMessage(new ChatComponentTranslation("tauradditions.swapper.linked"));
                }

                return true;
            } else if(held instanceof ItemRedstone){

                TileSwapper ts = (TileSwapper) worldIn.getTileEntity(pos);
                if(ts == null) return false;

                ts.toggle();

                return true;
            } else if(held instanceof ItemSign){

                TileSwapper ts = (TileSwapper) worldIn.getTileEntity(pos);
                if(ts == null) return false;

                playerIn.addChatComponentMessage(new ChatComponentText("Linked to: " + ts.getLinkPosition()));
                return true;
            }

        }

        return false;
    }

    @Override
    public TileEntity createTileEntity(World world, IBlockState state) {
        return new TileSwapper();
    }

    @Override
    public boolean hasTileEntity(IBlockState state) {
        return state.getBlock() instanceof BlockSwapper;
    }

}
