package io.tedsenft.tauradditions.common.blocks;

import io.tedsenft.tauradditions.TaurAdditions;
import io.tedsenft.tauradditions.TaurAdditionsTab;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;

/**
 * Created by Ted on 2/4/2016.
 */
public abstract class BlockMachine extends Block {

    public BlockMachine(){
        super(Material.iron);
        setCreativeTab(TaurAdditionsTab.instance);
    }
}
