package io.tedsenft.tauradditions.common.events;

import io.tedsenft.tauradditions.TaurAdditions;
import io.tedsenft.tauradditions.client.KeyBindings;
import io.tedsenft.tauradditions.common.blocks.BlockSwapper;
import io.tedsenft.tauradditions.common.items.ItemMultitool;
import io.tedsenft.tauradditions.common.packets.PacketModeChange;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by Ted on 1/27/2016.
 */
public class EventHandler {

    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    public void handleKeypress(InputEvent.KeyInputEvent ev){

        if(KeyBindings.modeChange.isPressed()){
            ItemStack is = Minecraft.getMinecraft().thePlayer.getCurrentEquippedItem();
            if(is != null && is.getItem() instanceof ItemMultitool){
                TaurAdditions.snw.sendToServer(new PacketModeChange(is));
            }
        }
    }

}
