package io.tedsenft.tauradditions.common.tiles;

import cofh.api.energy.EnergyStorage;
import cofh.api.energy.IEnergyProvider;
import cofh.api.energy.IEnergyReceiver;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IntegerCache;

/**
 * Created by Ted on 2/3/2016.
 */
public class TileCharger extends TileMachineBase implements IEnergyProvider {


    public TileCharger(){
        super();
        this.storage = new EnergyStorage(10000);
    }

    /**
     * Remove energy from an IEnergyProvider, internal distribution is left entirely to the IEnergyProvider.
     *
     * @param from       Orientation the energy is extracted from.
     * @param maxExtract Maximum amount of energy to extract.
     * @param simulate   If TRUE, the extraction will only be simulated.
     * @return Amount of energy that was (or would have been, if simulated) extracted.
     */
    @Override
    public int extractEnergy(EnumFacing from, int maxExtract, boolean simulate) {
        return 10000;
    }
}
