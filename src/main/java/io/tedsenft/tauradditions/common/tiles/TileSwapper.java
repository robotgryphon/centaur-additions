package io.tedsenft.tauradditions.common.tiles;

import cofh.api.energy.*;
import com.mojang.realmsclient.gui.ChatFormatting;
import io.tedsenft.tauradditions.LogHelper;
import io.tedsenft.tauradditions.utils.NBTUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.world.World;
import net.minecraftforge.client.event.MouseEvent;
import net.minecraftforge.fml.common.Optional;
import scala.tools.nsc.interpreter.Formatting;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ted on 1/28/2016.
 */
public class TileSwapper extends TileMachineBase implements ITickable {

    public enum TeleportResult {
        SUCCESS,
        NO_POWER,
        COOLDOWN,
        NOT_LINKED,
        NOT_ENABLED
    }

    protected BlockPos linkedTo;
    protected float cooldown;
    protected boolean enabled;

    public TileSwapper(){
        this.cooldown = 0f;
        this.linkedTo = null;
        this.enabled = true;
        this.storage = new EnergyStorage(100000, 1000, 0);
    }

    public boolean isEnabled(){
        return this.enabled;
    }

    public BlockPos getLinkPosition(){
        return linkedTo;
    }

    public void toggle(){
        this.enabled = !enabled;
        this.getTileData().setBoolean("enabled", this.enabled);
    }

    @Override
    public boolean canConnectEnergy(EnumFacing from) {
        return from == EnumFacing.DOWN;
    }

    public float getCooldown(){
        return this.cooldown;
    }

    /**
     * Like the old updateEntity(), except more generic.
     */
    @Override
    public void update() {
        if(this.cooldown > 0)
            this.cooldown -= 0.1f;

        if(this.cooldown < 0)
            this.cooldown = 0;
    }

    public void finishTeleport(){
        this.cooldown += 10.0f;
    }

    public TeleportResult tryTeleport(World world, EntityPlayer player){

        if(this.linkedTo == null)
            return TeleportResult.NOT_LINKED;

        TileSwapper linkedSwapper = (TileSwapper) world.getTileEntity(linkedTo);

        // If this or linked is not yet ready to go again
        if(this.cooldown > 0 || linkedSwapper.getCooldown() > 0)
            return TeleportResult.COOLDOWN;

        if(!this.enabled || !linkedSwapper.isEnabled())
            return TeleportResult.NOT_ENABLED;

        if(this.storage.getEnergyStored() < 1000)
            return TeleportResult.NO_POWER;

        storage.extractEnergy(1000, false);

        player.setPositionAndUpdate(linkedTo.getX() + 0.5f, linkedTo.getY() + 0.3, linkedTo.getZ() + 0.5f);
        this.cooldown += 10.0f;

        linkedSwapper.finishTeleport();


        return TeleportResult.SUCCESS;
    }

    public void setLinkPosition(BlockPos pos){
        this.linkedTo = pos;
        NBTTagCompound lt = NBTUtils.getTagFromPosition(pos);
        this.getTileData().setTag("linked_to", lt);
    }

    @Override
    public void writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);

        compound.setBoolean("enabled", this.enabled);

        compound.setFloat("cooldown", this.cooldown);

        // TODO: Error checking and validation
        if(linkedTo != null){
            NBTTagCompound pos = NBTUtils.getTagFromPosition(linkedTo);
            compound.setTag("linked_to", pos);
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);

        if(compound.hasKey("enabled"))
            this.enabled = compound.getBoolean("enabled");

        if(compound.hasKey("cooldown"))
            this.cooldown = compound.getFloat("cooldown");

        // TODO: Error checking and validation
        if(compound.hasKey("linked_to")){
            NBTTagCompound linked = compound.getCompoundTag("linked_to");
            BlockPos link = NBTUtils.getPositionFromTag(linked);

            this.linkedTo = link;
        }
    }
}
