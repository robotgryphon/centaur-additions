package io.tedsenft.tauradditions;

import net.minecraftforge.fml.common.FMLLog;
import org.apache.logging.log4j.Level;

/**
 * Created by Ted on 1/28/2016.
 */
public class LogHelper {

    public static void log(Level l, Object o){
        FMLLog.log(Reference.MOD_ID, l, "[" + Reference.MOD_NAME + "] %s", o.toString());
    }

    public static void info(Object o){
        log(Level.INFO, o);
    }

    public static void debug(Object o){
        log(Level.DEBUG, o);
    }

    public static void error(Object o){
        log(Level.ERROR, o);
    }
}
