package io.tedsenft.tauradditions;

import io.tedsenft.tauradditions.common.items.ItemHelper;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 * Created by Ted on 2/3/2016.
 */
public class TaurAdditionsTab extends CreativeTabs {

    public static TaurAdditionsTab instance;

    public TaurAdditionsTab(){
        super(Reference.MOD_NAME);
    }

    @Override
    public Item getTabIconItem() {
        return ItemHelper.tool;
    }
}
