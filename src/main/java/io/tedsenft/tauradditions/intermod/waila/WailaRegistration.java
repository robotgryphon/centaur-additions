package io.tedsenft.tauradditions.intermod.waila;

import io.tedsenft.tauradditions.common.blocks.BlockHelper;
import io.tedsenft.tauradditions.common.blocks.BlockSwapper;
import io.tedsenft.tauradditions.common.tiles.TileSwapper;
import mcp.mobius.waila.api.IWailaRegistrar;

/**
 * Created by Ted on 2/8/2016.
 */
public class WailaRegistration {

    private static WailaProviderSwapper swapper;

    public static void register(IWailaRegistrar reg){

        // Swapper block
        swapper = new WailaProviderSwapper();
        reg.registerNBTProvider(swapper, BlockSwapper.class);
        reg.registerBodyProvider(swapper, BlockSwapper.class);

    }
}
