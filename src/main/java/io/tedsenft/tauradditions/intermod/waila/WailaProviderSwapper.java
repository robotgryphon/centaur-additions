package io.tedsenft.tauradditions.intermod.waila;

import com.mojang.realmsclient.gui.ChatFormatting;
import io.tedsenft.tauradditions.LogHelper;
import io.tedsenft.tauradditions.common.tiles.TileSwapper;
import io.tedsenft.tauradditions.utils.NBTUtils;
import io.tedsenft.tauradditions.utils.PositionUtils;
import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import mcp.mobius.waila.api.IWailaDataProvider;
import net.minecraft.block.state.pattern.BlockHelper;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.client.event.RenderGameOverlayEvent;

import java.util.List;

/**
 * Created by Ted on 2/8/2016.
 */
public class WailaProviderSwapper implements IWailaDataProvider {
    @Override
    public ItemStack getWailaStack(IWailaDataAccessor iWailaDataAccessor, IWailaConfigHandler iWailaConfigHandler) {
         return iWailaDataAccessor.getStack();
    }

    @Override
    public List<String> getWailaHead(ItemStack itemStack, List<String> list, IWailaDataAccessor iWailaDataAccessor, IWailaConfigHandler iWailaConfigHandler) {
        return list;
    }

    @Override
    public List<String> getWailaBody(ItemStack itemStack, List<String> list, IWailaDataAccessor data, IWailaConfigHandler iWailaConfigHandler) {

        if(data.getNBTData() == null) return list;

        NBTTagCompound tags = data.getNBTData();

        if(tags.hasKey("cooldown"))
            list.add(ChatFormatting.RED + "Cooling down: " + tags.getFloat("cooldown"));

        if(!tags.getBoolean("enabled"))
            list.add(ChatFormatting.BOLD + "" + ChatFormatting.DARK_RED + "DISABLED");

        if(tags.getInteger("energy") > 0)
            list.add("Stored: " + ChatFormatting.GREEN + tags.getInteger("energy"));

        if(tags.hasKey("linked_to")) {
            BlockPos linked = NBTUtils.getPositionFromTag(tags.getCompoundTag("linked_to"));
            if(linked != null)
                list.add("Linked to: " + PositionUtils.formatPosition(linked, "x, y, z"));
        }

        return list;
    }

    @Override
    public List<String> getWailaTail(ItemStack itemStack, List<String> list, IWailaDataAccessor iWailaDataAccessor, IWailaConfigHandler iWailaConfigHandler) {
        return list;
    }

    @Override
    public NBTTagCompound getNBTData(EntityPlayerMP entityPlayerMP, TileEntity tileEntity, NBTTagCompound tag, World world, BlockPos blockPos) {

        NBTTagCompound data = tileEntity.getTileData();

        if(data.hasKey("cooldown"))
            tag.setFloat("cooldown", data.getFloat("cooldown"));

        if(data.hasKey("linked_to"))
            tag.setTag("linked_to", data.getCompoundTag("linked_to"));

        tag.setBoolean("enabled", data.getBoolean("enabled"));
        tag.setInteger("energy", data.getInteger("Energy"));

        return tag;
    }
}
