package io.tedsenft.tauradditions.intermod;

import io.tedsenft.tauradditions.LogHelper;
import net.minecraftforge.fml.common.event.FMLInterModComms;

/**
 * Created by Ted on 2/8/2016.
 */
public class ModHelper {
    public static void RegisterMods(){
        LogHelper.info("Registering third-party addons and mod support.");
        FMLInterModComms.sendMessage("Waila", "register", "io.tedsenft.tauradditions.intermod.waila.WailaRegistration.register");
    }
}
