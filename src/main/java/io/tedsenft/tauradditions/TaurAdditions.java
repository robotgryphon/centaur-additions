package io.tedsenft.tauradditions;

import io.tedsenft.tauradditions.common.blocks.BlockHelper;
import io.tedsenft.tauradditions.common.events.EventHandler;
import io.tedsenft.tauradditions.common.items.ItemHelper;
import io.tedsenft.tauradditions.common.items.multitool.MultitoolHelper;
import io.tedsenft.tauradditions.common.packets.PacketModeChange;
import io.tedsenft.tauradditions.intermod.ModHelper;
import io.tedsenft.tauradditions.proxy.IProxy;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.*;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = Reference.MOD_ID, version = Reference.MOD_VERSION, name = Reference.MOD_NAME, dependencies = Reference.Dependencies)
public class TaurAdditions
{

    @Mod.Instance(value = Reference.MOD_ID) //Tell Forge what instance to use.
    public static TaurAdditions instance;
    public static SimpleNetworkWrapper snw;
    private EventHandler eventHandler;

    @SidedProxy(clientSide = Reference.ClientProxy, serverSide = Reference.ServerProxy)
    public static IProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        // Register multitool functions
        MultitoolHelper.registerEmbeddedFunctions();

        snw = NetworkRegistry.INSTANCE.newSimpleChannel(Reference.MOD_ID);
        proxy.registerPackets();

        TaurAdditionsTab.instance = new TaurAdditionsTab();

        ItemHelper.registerItems();

        BlockHelper.registerBlocks();

        proxy.registerKeybindings();
        proxy.registerTileEntities();
        proxy.initRenderingAndTextures();

        eventHandler = new EventHandler();
        MinecraftForge.EVENT_BUS.register(eventHandler);

        ModHelper.RegisterMods();
    }

    @Mod.EventHandler
    public void load(FMLInitializationEvent event)
    {
        ItemHelper.createRecipes();
        BlockHelper.createRecipes();
    }
}
