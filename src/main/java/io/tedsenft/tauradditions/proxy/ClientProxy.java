package io.tedsenft.tauradditions.proxy;

import io.tedsenft.tauradditions.Reference;
import io.tedsenft.tauradditions.client.KeyBindings;
import io.tedsenft.tauradditions.common.blocks.BlockHelper;
import io.tedsenft.tauradditions.common.items.ItemHelper;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.model.b3d.B3DLoader;
import net.minecraftforge.client.model.obj.OBJLoader;

/**
 * Created by Ted on 1/28/2016.
 */
public class ClientProxy extends CommonProxy {

    @Override
    public ClientProxy getClientProxy() {
        return this;
    }

    @Override
    public void initRenderingAndTextures() {
        ItemHelper.registerItemModels();
        BlockHelper.registerBlockModels();

        B3DLoader.instance.addDomain(Reference.MOD_ID);
        OBJLoader.instance.addDomain(Reference.MOD_ID);
    }

    @Override
    public void registerKeybindings() {
        KeyBindings.registerKeys();
    }

    @Override
    public void playSound(String soundName, float xCoord, float yCoord, float zCoord, float volume, float pitch) {

    }

    @Override
    public void spawnParticle(String particleName, double xCoord, double yCoord, double zCoord, double xVelocity, double yVelocity, double zVelocity) {

    }
}
