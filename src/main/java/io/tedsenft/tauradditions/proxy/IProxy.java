package io.tedsenft.tauradditions.proxy;

/**
 * Created by Ted on 1/28/2016.
 */
public interface IProxy {
    public abstract ClientProxy getClientProxy();

    public abstract void registerTileEntities();

    public abstract void initRenderingAndTextures();

    public abstract void registerEventHandlers();

    public abstract void registerKeybindings();

    public abstract void playSound(String soundName, float xCoord, float yCoord, float zCoord, float volume, float pitch);

    public abstract void spawnParticle(String particleName, double xCoord, double yCoord, double zCoord, double xVelocity, double yVelocity, double zVelocity);

    public abstract void registerPackets();
}
