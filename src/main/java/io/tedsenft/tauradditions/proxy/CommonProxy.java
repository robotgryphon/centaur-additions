package io.tedsenft.tauradditions.proxy;

import io.tedsenft.tauradditions.LogHelper;
import io.tedsenft.tauradditions.Reference;
import io.tedsenft.tauradditions.TaurAdditions;
import io.tedsenft.tauradditions.common.packets.PacketModeChange;
import io.tedsenft.tauradditions.common.tiles.TileCharger;
import io.tedsenft.tauradditions.common.tiles.TileEnergyNode;
import io.tedsenft.tauradditions.common.tiles.TileSwapper;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;

/**
 * Created by Ted on 1/28/2016.
 */
public abstract class CommonProxy implements IProxy {

    @Override
    public void registerTileEntities() {
        LogHelper.info("Registering tiles.");

        GameRegistry.registerTileEntity(TileSwapper.class, Reference.MOD_ID + ":te_swapper");
        GameRegistry.registerTileEntity(TileCharger.class, Reference.MOD_ID + ":te_charger");
        GameRegistry.registerTileEntity(TileEnergyNode.class, Reference.MOD_ID + ":te_energy_node");
    }

    @Override
    public void registerEventHandlers() {

    }

    @Override
    public void registerPackets(){
        SimpleNetworkWrapper snw = TaurAdditions.snw;

        snw.registerMessage(PacketModeChange.Handler.class, PacketModeChange.class, 0, Side.SERVER);
    }
}
