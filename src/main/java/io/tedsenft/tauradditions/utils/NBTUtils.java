package io.tedsenft.tauradditions.utils;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.BlockPos;

/**
 * Created by Ted on 2/8/2016.
 */
public class NBTUtils {

    public static BlockPos getPositionFromTag(NBTTagCompound comp){
        if(comp == null)
            return null;

        if(!(comp.hasKey("x") && comp.hasKey("y") && comp.hasKey("z")))
            return null;

        BlockPos ret = new BlockPos(
                comp.getInteger("x"),
                comp.getInteger("y"),
                comp.getInteger("z")
        );

        return ret;
    }

    public static NBTTagCompound getTagFromPosition(BlockPos position){

        NBTTagCompound pos = new NBTTagCompound();
        pos.setInteger("x", position.getX());
        pos.setInteger("y", position.getY());
        pos.setInteger("z", position.getZ());

        return pos;
    }
}
