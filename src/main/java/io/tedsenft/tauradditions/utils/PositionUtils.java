package io.tedsenft.tauradditions.utils;

import net.minecraft.util.BlockPos;

/**
 * Created by Ted on 2/12/2016.
 */
public class PositionUtils {

    public static String formatPosition(BlockPos pos, String format){
        if(format == null)
            format = "X, Y, Z";

        format = format.replaceAll("[xX]", String.valueOf(pos.getX()));
        format = format.replaceAll("[Yy]", String.valueOf(pos.getY()));
        format = format.replaceAll("[Zz]", String.valueOf(pos.getZ()));

        return format;
    }
}
