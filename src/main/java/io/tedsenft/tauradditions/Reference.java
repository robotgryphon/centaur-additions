package io.tedsenft.tauradditions;

/**
 * Created by Ted on 1/28/2016.
 */
public class Reference {

    public static final String MOD_NAME = "Centaur Additions";
    public static final String MOD_VERSION = "1.0.0";
    public static final String MOD_ID = "tauradditions";

    public static final String ClientProxy = "io.tedsenft.tauradditions.proxy.ClientProxy";
    public static final String ServerProxy = "io.tedsenft.tauradditions.proxy.ServerProxy";

    public static final float OneSixteenth = 1f / 16f;

    public static final String Dependencies = "required-after:Baubles";
}
