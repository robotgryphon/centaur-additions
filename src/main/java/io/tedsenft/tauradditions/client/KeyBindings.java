package io.tedsenft.tauradditions.client;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import org.lwjgl.input.Keyboard;

/**
 * Created by Ted on 2/8/2016.
 */
public class KeyBindings {

    public static KeyBinding modeChange;

    public static void registerKeys(){
        modeChange = new KeyBinding("Mode Change", Keyboard.KEY_NONE, "key.categories.tauradditions");

        ClientRegistry.registerKeyBinding(modeChange);
    }
}
